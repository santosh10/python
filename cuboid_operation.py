class Cuboid:
    l = None
    b = None
    h = None

    def volume(self):
        return self.l * self.b * self.h

    def tsa(self):
        return self.l * self.b * self.h + 2 * (self.l + self.b) * self.h


sample_cuboid = Cuboid()
sample_cuboid.l = 3
sample_cuboid.b = 4
sample_cuboid.h = 5

print("volume of cuboid is:", sample_cuboid.volume())
print("TSA of cuboid is :", sample_cuboid.tsa())
