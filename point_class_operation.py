import math


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return 'Point(%d,%d)' % (self.x, self.y)


a = Point(4, 5)
b = Point(5, 7)
print(a)
distance = math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2)
print("Distance between a and b is :", distance)

print("\n" * 2)


# operation on rectangle
class Rectangle:
    def __init__(self, l, b):
        self.l = l
        self.b = b

    def area(self):
        return self.l * self.b

    def perimeter(self):
        return 2 * (self.l * self.b)

    def square(self):
        return self.l == self.b

    def diagonal(self):
        return math.sqrt(self.l ** 2 + self.b ** 2)


sample_rectangle = Rectangle(5, 6)
print("area is:", sample_rectangle.area())
print("perimeter is:", sample_rectangle.perimeter())
print("square is:", sample_rectangle.square())
print("diagonal:", sample_rectangle.diagonal())

print("\n" * 2)
# operation on triangle
class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def area(self):
        s = (self.a + self.b + self.c) / 2
        return math.sqrt((s * (s - self.a) * (s - self.b) * (s - self.c)))

    def equilateral(self):
        return self.a == self.b == self.c

    def isosceles(self):
        if self.a == self.b or self.b == self.c or self.a == self.c:
            return "isosceles"
        else:
            return False


sample_triangle = Triangle(4, 6, 4)
print("area of triangle is:", sample_triangle.area())
print("equilater triangle:", sample_triangle.equilateral())
print("isosceles triangle : ", sample_triangle.isosceles())

print("\n" * 2)
# circle_operation
class Circle:
    def __init__(self, r):
        self.r = r

    def area(self):
        return math.pi * self.r ** 2

    def circumference(self):
        return 2 * math.pi * self.r

    def diagonal(self):
        return 2 * self.r


sample_circle = Circle(7)
print("the area of circle is:", sample_circle.area())
print("the circumference of circle is:", sample_circle.circumference())
print("the diagonal of circle is", sample_circle.diagonal())

print("\n" * 2)
# operation_cuboid
class Cuboid:
    def __init__(self, l, b, h):
        self.l = l
        self.b = b
        self.h = h

    def volume(self):
        return self.l * self.b * self.h

    def tsa(self):
        return self.l * self.b * self.h + 2 * (self.l + self.b) * self.h


sample_cuboid = Cuboid(4, 5, 6)
print("volume of cuboid is:", sample_cuboid.volume())
print("TSA of cuboid is :", sample_cuboid.tsa())
