class Employee:
    def __init__(self, name, salary, address): #to remove duplicate attributes
        self.name = name
        self.salary = salary
        self.address = address

    def calculate_tax(self):
        return self.salary * 0.01  # to remove duplicate functions


class Teacher(Employee):
    def __init__(self, name, address, full_time, salary, subject):
        self.full_time = full_time
        self.subuject = subject
        super().__init__(name, address, salary)

    def take_class(self):
        print('class taken')


class Helper(Employee):
    def __init__(self, name, address, salary):
        super().__init__(name, address, salary)

    def clean_desk(self):
        print('clean desk')


class Manager(Employee):
    def __init__(self, name, department, salary, address):
        self.department = department
        super().__init__(name, address, salary)

    def task_assigned(self):
        print('Task Assigned')


sample_teacher = Teacher('santosh khanal', 'walling', True, 200000, 'theory of computation')
sample_helper = Helper('kale kancha', 'syangja', 100000)
sample_manager = Manager('khanal santosh', 'web technology', 300000, 'gandaki')
sample_teacher.take_class()
print(" Task amoount of teacher is : ", sample_teacher.calculate_tax())
sample_helper.clean_desk()
print(" Task amount of helper is : ", sample_helper.calculate_tax())
sample_manager.task_assigned()
print(" Task amount of manager is : ", sample_manager.calculate_tax())
