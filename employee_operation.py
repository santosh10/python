class Employee:
    Name = None
    Gender = None
    Address = None
    Mobile = None
    Married = None
    Yearly_income = None

    def calculate_tax(self):
        if self.Married == True and self.Yearly_income > 4500000:
            taxable_amount = self.Yearly_income - 4500000
            return taxable_amount * 0.15
        elif self.Married == False and self.Yearly_income > 3500000:
            taxable_amount = self.Yearly_income - 4500000
            return taxable_amount * 0.15
        else:
            return self.Yearly_income * 0.01


santosh = Employee()
santosh.Name = 'Santosh khanal'
santosh.Address = 'syangja'
santosh.Mobile = '9866061070'
santosh.Married = True
santosh.Yearly_income = 650000

print("Name:", santosh.Name)
print("Address:", santosh.Address)
print("Mobile:", santosh.Mobile)
print("Martial status:", santosh.Married)
print("Yearly income is:", santosh.Yearly_income)
print("Tax:", santosh.calculate_tax())
