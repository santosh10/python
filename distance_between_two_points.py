import math


# def distance(x1, x2, y1, y2):
#     return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
#
#
# print(distance(2, 4, 2, 4))


# class point

class Point:  # class vitra function ni banauna milxa variables ni banauna milxa
    X= None
    Y = None

    def quadrant(self):
        if self.X == 0 and self.Y == 0:
            return None
        if self.X > 0 and self.Y > 0:
            return 1
        if self.X < 0 and self.Y > 0:
            return 2
        if self.X < 0 and self.Y < 0:
            return 3
        if self.X > 0 and self.Y < 0:
            return 4


a = Point()
a.X = 5
a.Y = 3

b = Point()
b.X = -7
b.Y = -10

c = Point()
c.X = -8
c.Y = 4

d = Point()
d.X = 2
d.Y = -9

s = Point()
s.X = 0
s.Y = 0

distance = math.sqrt((a.X - b.X) ** 2 + (a.Y - b.Y) ** 2)
print("Distance is ", distance)
# print(type(a))  # a ko type k ho
# print(type(b))

print('a is in:', a.quadrant(), 'quadrant')
print('b is in:', b.quadrant(), 'quadrant')
print('c is in:', c.quadrant(), 'quadrant')
print('d is in:', d.quadrant(), 'quadrant')
print('s is in:', s.quadrant(), 'quadrant')
