import math


class Triangle:
    a = None
    b = None
    c = None

    def area(self):
        s = (self.a + self.b + self.c) / 2
        return math.sqrt((s * (s - self.a) * (s - self.b) * (s - self.c)))

    def equilater(self):
        return self.a == self.b == self.c

    def isosceles(self):
        if self.a == self.b or self.b == self.c or self.a == self.c:
            return "isosceles"
        else:
            return False


sample_triangle = Triangle()
sample_triangle.a = 3
sample_triangle.b = 4
sample_triangle.c = 5

print("area of triangle is:", sample_triangle.area())
print("equilater triangle:", sample_triangle.equilater())
print("isosceles triangle : ", sample_triangle.isosceles())
