
class Teacher:
    def __init__(self, name, address, full_time, salary, subject):
        self.name = name
        self.address = address
        self.full_time = full_time
        self.salary = salary
        self.subuject = subject

    def calculate_tax(self):
        if self.salary > 200000:
            taxable_amount = self.salary - 200000
            return taxable_amount * 0.15
        else:
            return self.salary * 0.01

    def take_class(self):
        print('class taken')


class Helper:
    def __init__(self, name, address, salary):
        self.name = name
        self.address = address
        self.salary = salary

    def calculate_tax(self):
        if self.salary > 200000:
            taxable_amount = self.salary - 200000
            return taxable_amount * 0.15
        else:
            return self.salary * 0.01

    def clean_desk(self):
        print('clean desk')


class Manager:
    def __init__(self, name, department, salary, address):
        self.name = name
        self.department = department
        self.salary = salary
        self.address = address

    def calculate_tax(self):
        if self.salary > 200000:
            taxable_amount = self.salary - 200000
            return taxable_amount * 0.15
        else:
            return self.salary * 0.01

    def task_assigned(self):
        print('Task Assigned')


sample_teacher = Teacher('santosh khanal', 'walling', True, 200000, 'theory of computation')
sample_helper = Helper('kale kancha', 'syangja', 100000)
sample_manager = Manager('khanal santosh', 'web technology', 300000, 'gandaki')
sample_teacher.take_class()
print(" Task amoount of teacher is : ", sample_teacher.calculate_tax())
sample_helper.clean_desk()
print(" Task amount of helper is : ", sample_helper.calculate_tax())
sample_manager.task_assigned()
print(" Task amount of manager is : ", sample_manager.calculate_tax())