import math


class Circle:
    r = None

    def area(self):
        return math.pi * self.r ** 2

    def circumference(self):
        return 2 * math.pi * self.r

    def diagonal(self):
        return 2 * self.r


sample_circle = Circle()
sample_circle.r = 7

print(sample_circle.area())
print(sample_circle.circumference())
print(sample_circle.diagonal())
