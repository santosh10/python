class Time:
    def __init__(self, hr, min, sec):
        self.hr = hr
        self.min = min
        self.sec = sec

    def __str__(self):
        return '%d:%d:%d' % (self.hr, self.min, self.sec)

    def __add__(self, other):
        sec= self.sec + other.sec
        if sec >= 60:
            minute = self.min + other.min + 1
            sec=sec-60
        else:
            minute=self.min+other.min
        if minute >= 60:
            hr = self.hr + other.hr + 1
            minute=minute-60
        else:
            hr=self.hr+other.hr

         return Time(hr, minute, sec)


a = Time(10, 35, 40)
b = Time(2, 30, 5)
print(a)
print(b)
c = a + b
print(c)

c = a + b
