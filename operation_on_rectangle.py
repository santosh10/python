import math


class Rectangle:
    length = None
    breadth = None

    def area(self):
        return self.length * self.breadth

    def perimeter(self):
        return 2 * (self.length * self.breadth)

    def square(self):
        return self.length == self.breadth

    def diagonal(self):
        return math.sqrt(self.length ** 2 + self.breadth ** 2)


sample_rect = Rectangle()
sample_rect.length = 3
sample_rect.breadth = 4

print(sample_rect.area())
print(sample_rect.perimeter())
print(sample_rect.square())
print(sample_rect.diagonal())
